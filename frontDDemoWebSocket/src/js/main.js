let stompCliente = null;
export const onWebSocketClose = () => {
  if (stompCliente !== null) {
    stompCliente.deactivate();
  }
};
const onConnectSocket = () => {
  stompCliente.subscribe(`/topic/messages`, (mensaje) => {
    mostrarMensaje(mensaje.body);
    console.log(JSON.parse(mensaje.body));
  });
};
export const conectarWS = (endpoint) => {
  stompCliente = new StompJs.Client({
    webSocketFactory: () => new WebSocket(`ws://localhost:8080/ws${endpoint}`),
  });
  stompCliente.onConnect = onConnectSocket;
  stompCliente.onWebSocketClose = onWebSocketClose;
  stompCliente.activate();
};

export const enviarMensaje = (id) => {
  let txtMensaje = document.getElementById("chat");

  stompCliente.publish({
    destination: `/app/chat/${id}`,
    body: JSON.stringify({
      name: "packo",
      content: txtMensaje.value,
      messages: "chat1",
    }),
  });

  console.log(txtMensaje.value);
};
const mostrarMensaje = (mensaje) => {
  const body = JSON.parse(mensaje);

  const UlMensajes = document.getElementById("ULmensajes");

  const mensajeLI = document.createElement("li");
  mensajeLI.innerHTML = `<strong>${body.name}</strong> : ${body.content}`;
  UlMensajes.appendChild(mensajeLI);
};
