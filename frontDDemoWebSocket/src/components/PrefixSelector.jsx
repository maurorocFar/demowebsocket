import { useState } from 'react';



export default function PrefixSelector({ onPrefixChange }) {
  const [prefix, setPrefix] = useState('');

  const handleInputChange = (event) => {
    setPrefix(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    onPrefixChange(prefix);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Topic:
        <input type="text" value={prefix} onChange={handleInputChange} />
      </label>
      <button type="submit">Actualizar Prefijo</button>
    </form>
  );
}
