export const JoinChat = () => {
  return (
    <div className="h-[800px]">
      <h1 className="text-5xl text-white mb-4">
        ¡Ingresá en un chat con tu código!
      </h1>

      <form className="max-h-[400px] border-2 flex flex-col gap-4 p-10 rounded-lg justify-between bg-[rgb(0,0,0,0.2)] mt-32">
        <section>
          <label htmlFor="NameChat" className="block text-white text-xl mb-1">
            Código del chat
          </label>
          <input
            type="text"
            id="NameChat"
            className="w-full py-1 px-2 rounded-sm bg-transparent border text-white focus:outline-none "
          />
        </section>
        <section>
          <label htmlFor="TemaChat" className="block text-white text-xl mb-1">
            Nickname
          </label>
          <input
            type="text"
            id="TemaChat"
            className="w-full py-1 px-2 rounded-sm bg-transparent border text-white focus:outline-none "
          />
        </section>

        <button
          className="w-fit m-auto mt-4 cursor-pointer inline-flex items-center rounded-full px-9 py-3 text-xl font-mono font-semibold text-green-600 hover:text-white border-2 border-green-600
hover:bg-green-600 transition ease-in-out   duration-200  focus:bg-transparent"
        >
          Unirme
        </button>
      </form>
    </div>
  );
};
