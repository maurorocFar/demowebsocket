export const NewChat = () => {
  const newChat = async () => {
    const request = await fetch("http://localhost:8080/generateconetion", {
      method: "POST",
      headers: {
        "Content-Type": "Application/json",
      },
      mode: "no-cors",
    });
    console.log(request);
    const data = await request.json();
    console.log(data);
  };
  return (
    <div className="h-[800px]">
      <h1 className="text-5xl text-white mb-4">
        ¿Quierés crear un nuevo chat?
      </h1>

      <p className="text-gray-300 text-2xl mb-20">
        Recuerda que los chats sólo perduran por 1 día.
      </p>
      <form className="max-h-[400px] border-2 flex flex-col gap-4 p-10 rounded-lg justify-between bg-[rgb(0,0,0,0.2)]">
        <section>
          <label htmlFor="NameChat" className="block text-white text-xl mb-1">
            Nombre del chat
          </label>
          <input
            type="text"
            id="NameChat"
            className="w-full py-1 px-2 rounded-sm bg-transparent border text-white focus:outline-none "
          />
        </section>

        <section>
          <label htmlFor="NickName" className="block text-white text-xl mb-1">
            Nickname
          </label>
          <input
            type="text"
            id="NickName"
            className="w-full py-1 px-2 rounded-sm bg-transparent border text-white focus:outline-none "
          />
        </section>

        <button
          onClick={(e) => {
            e.preventDefault();
            newChat();
          }}
          className="w-fit m-auto mt-4 cursor-pointer inline-flex items-center rounded-full px-9 py-3 text-xl font-mono font-semibold text-rose-600 hover:text-white border-2 border-rose-600
hover:bg-rose-600 transition ease-in-out   duration-200  focus:bg-transparent"
        >
          Crear
        </button>
      </form>
    </div>
  );
};
