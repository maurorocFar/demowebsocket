import { useState } from "react";
import TheChat from "../components/TheChat";

export const Chats = () => {
  console.log("renderizado");
  const [activeChat, setActiveChat] = useState(false);
  const [endPoint, setEndPoint] = useState("");

  const Chats = [
    { endpoint: "", id: "12" },
    { endpoint: "/hola", id: "123" },
  ];

  const connectChat = (endpoint) => {
    setEndPoint(endpoint);
    setActiveChat(!activeChat);
  };
  return (
    <div className="w-full h-full text-center">
      <h1 className="text-5xl text-white mb-4 ">Active chats</h1>

      <main className="px-10 grid grid-cols-3 gap-5">
        {Chats.map((endpoint) => {
          return (
            <div className="border-2 p-10" key={endpoint.id}>
              <button onClick={() => connectChat(endpoint)}>abreme we</button>
            </div>
          );
        })}

        {activeChat && (
          <TheChat
            activeChat={activeChat}
            setActiveChat={setActiveChat}
            endpoint={endPoint}
          />
        )}
      </main>
    </div>
  );
};
