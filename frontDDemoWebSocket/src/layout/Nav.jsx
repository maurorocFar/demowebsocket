import { NavLink } from "react-router-dom";

export const Nav = () => {
  return (
    <nav className="h-[90px] w-full mb-10 pt-10">
      <ul className="w-full h-full flex justify-around items-center ">
        <li className="text-2xl">
          <NavLink
            to="/Inicio"
            className={({ isActive }) =>
              isActive
                ? "border-2  border-[#3E84F0] relative text-white p-1 px-80 rounded-lg bg-[#3E84F0] transition-all -ml-32 duration-500  before:absolute before:top-1 before:left-0 before:w-full before:h-full before:bg-[#3E84F0] before:blur-md "
                : "border-2 p-1 px-6 rounded-lg bg-white transition-all duration-500"
            }
          >
            <ion-icon name="home-outline"></ion-icon>
            <span className="z-20 relative"> Inicio</span>
          </NavLink>
        </li>

        <li className="text-2xl ">
          <NavLink
            to="/Chats"
            className={({ isActive }) =>
              isActive
                ? "border-2 border-[#BB2BE1] relative text-white p-1 px-96 rounded-lg bg-[#BB2BE1] transition-all ml-32 duration-500 before:absolute before:top-1 before:left-0 before:w-full before:h-full before:bg-[#BB2BE1] before:blur-md "
                : "border-2 p-1 px-6 rounded-lg bg-white transition-all duration-500"
            }
          >
            <ion-icon name="code-slash-outline"></ion-icon>
            <span className="z-20 relative"> Chats</span>
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};
