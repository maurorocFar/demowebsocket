import { NewChat } from "./NewChat";

import { JoinChat } from "./JoinChat";
export const Home = () => {
  return (
    <main className="flex justify-around">
      <NewChat />
      <JoinChat />
    </main>
  );
};
