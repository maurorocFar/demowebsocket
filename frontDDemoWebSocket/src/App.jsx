import { PrincipalRouter } from "./router/PrincipalRouter";
function App() {
  return (
    <>
      <PrincipalRouter />
    </>
  );
}

export default App;
