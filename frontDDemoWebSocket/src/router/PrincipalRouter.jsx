import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

import { Home } from "../layout/Home";
import { Chats } from "../layout/Chats";
import { Nav } from "../layout/Nav";
export const PrincipalRouter = () => {
  return (
    <BrowserRouter>
      <main className="min-h-[100vh] items-center bg-gradient-to-r from-purple-600 to-blue-600">
        <Nav />
        <Routes>
          <Route path="/" element={<Navigate to="/Inicio" />} />
          <Route path="/Inicio" element={<Home />} />
          <Route path="/Chats" element={<Chats />} />
        </Routes>
      </main>
    </BrowserRouter>
  );
};
