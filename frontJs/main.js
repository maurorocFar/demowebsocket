let stompCliente = null;
const onConnectSocket = () => {
  stompCliente.subscribe("/topic/messages", (mensaje) => {
    mostrarMensaje(mensaje.body);
    console.log(JSON.parse(mensaje.body));
  });
};

const onWebSocketClose = () => {
  if (stompCliente !== null) {
    stompCliente.deactivate();
  }
};
const conectarWS = () => {
  onWebSocketClose();
  stompCliente = new StompJs.Client({
    webSocketFactory: () => new WebSocket("ws://localhost:8080/ws"),
  });
  stompCliente.onConnect = onConnectSocket;
  stompCliente.onWebSocketClose = onWebSocketClose;
  stompCliente.activate();
};
const enviarMensaje = () => {
  let txtNombre = document.getElementById("name");

  let txtMensaje = document.getElementById("messages");

  stompCliente.publish({
    destination: "/app/chat",
    body: JSON.stringify({
      name: txtNombre.value,
      content: txtMensaje.value,
    }),
  });

  console.log(txtMensaje.value);
};
const mostrarMensaje = (mensaje) => {
  const body = JSON.parse(mensaje);

  const UlMensajes = document.getElementById("ULmensajes");

  const mensajeLI = document.createElement("li");
  mensajeLI.innerHTML = `<strong>${body.name}</strong> : ${body.content}`;
  UlMensajes.appendChild(mensajeLI);
};

document.addEventListener("DOMContentLoaded", () => {
  const btnEnviar = document.getElementById("enviarMensaje");
  btnEnviar.addEventListener("click", (e) => {
    e.preventDefault();
    enviarMensaje();
  });
  conectarWS();
});
