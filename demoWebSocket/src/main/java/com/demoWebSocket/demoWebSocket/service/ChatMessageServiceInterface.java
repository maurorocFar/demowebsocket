package com.demoWebSocket.demoWebSocket.service;

import com.demoWebSocket.demoWebSocket.entity.ChatMessage;

import java.util.Date;
import java.util.List;

public interface ChatMessageServiceInterface {

    public void saveMessage(ChatMessage message);

    public List<ChatMessage> getAllMessages();

    public void deleteAllMessages(Date dateCurrent);


    public List<ChatMessage> getMessagesByTopic(String topic);
}
