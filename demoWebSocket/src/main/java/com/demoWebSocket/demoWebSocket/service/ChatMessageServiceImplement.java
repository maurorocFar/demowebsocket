package com.demoWebSocket.demoWebSocket.service;

import com.demoWebSocket.demoWebSocket.entity.ChatMessage;
import com.demoWebSocket.demoWebSocket.repository.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ChatMessageServiceImplement implements ChatMessageServiceInterface{

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Override
    public void saveMessage(ChatMessage message) {
        try{
            chatMessageRepository.save(message);
        }catch (Exception e){
            throw new RuntimeException("Hubo un error al persistir ChatMessage a la base de datos");
        }
    }

    @Override
    public List<ChatMessage> getAllMessages() {
        try {
            List<ChatMessage> listaMessage =   chatMessageRepository.findAll();
            return listaMessage;
        }catch (Exception e){
            throw new RuntimeException("Error al traer la lista de ChatMessage");
        }
    }

    @Override
    public void deleteAllMessages(Date dateCurrent) {
            try{
                chatMessageRepository.deleteByMessageTimeBefore(dateCurrent);
            }catch (Exception e){
                throw new RuntimeException("Error al eliminar los mensajes");
            }
    }

    @Override
    public List<ChatMessage> getMessagesByTopic(String topic) {
        List<ChatMessage> listaMensajesPorTopic = chatMessageRepository.findByTopic(topic);
        return listaMensajesPorTopic;
    }
}
