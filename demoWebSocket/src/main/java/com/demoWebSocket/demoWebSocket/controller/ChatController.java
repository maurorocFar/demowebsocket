package com.demoWebSocket.demoWebSocket.controller;
import com.demoWebSocket.demoWebSocket.config.WebSocketConfig;
import com.demoWebSocket.demoWebSocket.entity.ChatMessage;
import com.demoWebSocket.demoWebSocket.service.ChatMessageServiceInterface;
import com.mysql.cj.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;


@Controller
public class ChatController {


    @Autowired
    private ChatMessageServiceInterface chatMessageServiceInterface;




    @MessageMapping("/chat/{messages}")
    @SendTo("/topic/messages")
    public ChatMessage sendMessage(@DestinationVariable String messages, @RequestBody ChatMessage message) throws InterruptedException {

        System.out.println(messages);
        //hora y fecha actual
        LocalDateTime now = LocalDateTime.now(ZoneId.of("America/Argentina/Buenos_Aires"));
        Date currentDateTime = java.sql.Timestamp.valueOf(now);
        System.out.println(message.getContent());
        message.setMessageTime(currentDateTime);
        Thread.sleep(1000); // simulated delay
        chatMessageServiceInterface.saveMessage(message);
        return message;
    }






}







