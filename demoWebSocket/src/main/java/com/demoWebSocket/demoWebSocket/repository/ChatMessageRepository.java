package com.demoWebSocket.demoWebSocket.repository;

import com.demoWebSocket.demoWebSocket.entity.ChatMessage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {



    void deleteByMessageTimeBefore(Date messageTime);


    List<ChatMessage> findByTopic(String topic);
}
