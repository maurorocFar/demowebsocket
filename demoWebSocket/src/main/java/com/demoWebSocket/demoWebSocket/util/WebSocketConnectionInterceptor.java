package com.demoWebSocket.demoWebSocket.util;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;

public class WebSocketConnectionInterceptor implements ChannelInterceptor {


    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if (accessor != null) {
            switch (accessor.getCommand()) {
                case CONNECT:
                    // Se ha establecido una nueva conexión
                    System.out.println("Se ha establecido una nueva conexión WebSocket");
                    break;
                case DISCONNECT:
                    // Se ha cerrado una conexión
                    System.out.println("Se ha cerrado una conexión WebSocket");
                    break;
                default:
                    break;
            }
        }

        return message;
    }
}
