package com.demoWebSocket.demoWebSocket.scheduleTask;

import com.demoWebSocket.demoWebSocket.service.ChatMessageServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class ScheduledTask {

    @Autowired
    private ChatMessageServiceInterface messageServiceInterface;

    @Scheduled(cron = "0 0 3 * * *", zone = "America/Argentina/Buenos_Aires")
    public void deleteAllMessagesDaily() {

        // Obtener la hora y fecha actual en Argentina
        LocalDateTime now = LocalDateTime.now(ZoneId.of("America/Argentina/Buenos_Aires"));
        Date currentDateTime = java.sql.Timestamp.valueOf(now);
        messageServiceInterface.deleteAllMessages(currentDateTime);
    }
}
